import './App.css';
import Chat from './components/Chat';
import MainFooter from './components/MainFooter/MainFooter';
import MainHeader from './components/MainHeader/MainHeader';
import { BASE_URL } from './constants/api';

const App = () => {
  return (
    <div className="App">
      <MainHeader />
      <Chat url={BASE_URL} />
      <MainFooter />
    </div>
  );
};

export default App;
