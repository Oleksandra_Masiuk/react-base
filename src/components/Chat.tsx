import propTypes from 'prop-types';
import { useEffect, useState, FC } from 'react';
import { v4 as uuid } from 'uuid';

import Preloader from './Preloader/Preloader';
import MessageList from './MessageList/MessageList';
import MessageInput from './MessageInput/MessageInput';
import Header from './Header/Header';
import { IMessage } from '../interfaces/interfaces';

interface ChatProps {
  url: string;
}

const Chat: FC<ChatProps> = ({ url }) => {
  const [messages, setMessages] = useState([]);
  const [userCounter, setUserCounter] = useState(0);
  const [messageCounter, setMessageCounter] = useState(0);
  const [lastMessageDate, setLastMessageDate] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [editedMessage, setEditedMessage] = useState(null);

  useEffect(() => {
    const fetchMessages = async () => {
      setIsLoading(true);

      const res = await fetch(url);
      if (!res.ok) {
        setIsLoading(false);
        return console.error('No messages found');
      }
      const data = await res.json();

      const users = new Map();
      data.forEach((message: IMessage) => {
        users.set(
          message.user,
          users.get(message.user) ? 1 + users.get(message.user) : 1
        );
      });

      data.sort(
        (a: IMessage, b: IMessage) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
      );

      const lastDate = data[data.length - 1].createdAt;

      setMessages([...data]);
      setMessageCounter(data.length);
      setUserCounter(users.size);
      setLastMessageDate(lastDate ? lastDate : '');
      setIsLoading(false);
    };
    fetchMessages();
  }, [url]);

  const addNewMessage = (message: IMessage): void => {
    const newMessages = [...messages, { ...message, id: uuid() }];
    setMessages(newMessages);
    const users = new Map();
    newMessages.forEach((message: IMessage) => {
      users.set(
        message.user,
        users.get(message.user) ? 1 + users.get(message.user) : 1
      );
    });
    setUserCounter(users.size);
    const date = message.createdAt.toString();
    setLastMessageDate(date ? date : '');
    setMessageCounter((messageCounter) => messageCounter + 1);
  };

  const setMessageToEdit = (id: string): void => {
    const message = messages.find((mes) => mes.id === id);
    setEditedMessage(message);
  };

  const editMessage = (text: string): void => {
    if (editedMessage.text === text) return;
    const filteredMessages = messages.filter(
      (mes) => mes.id !== editedMessage.id
    );
    setMessages(
      [
        ...filteredMessages,
        { ...editedMessage, text, editedAt: new Date().toString() },
      ].sort(
        (a: IMessage, b: IMessage) =>
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime()
      )
    );
    setEditedMessage(null);
  };

  const deleteMessage = (id: string): void => {
    const newMessages = messages.filter((mes) => mes.id !== id);
    setMessages(newMessages);
    const users = new Map();
    newMessages.forEach((message: IMessage) => {
      users.set(
        message.user,
        users.get(message.user) ? 1 + users.get(message.user) : 1
      );
    });
    setMessageCounter((messageCounter) => messageCounter - 1);
    setUserCounter(users.size);
    const lastDate = newMessages[newMessages.length - 1].createdAt;
    setLastMessageDate(lastDate ? lastDate : '');
  };

  return (
    <div className="chat">
      {isLoading ? (
        <Preloader />
      ) : (
        <>
          <Header
            userCounter={userCounter}
            messageCounter={messageCounter}
            lastMessageDate={lastMessageDate}
          />
          <MessageList
            messages={messages}
            editedMessage={editedMessage ? editedMessage.id : null}
            setMessageToEdit={setMessageToEdit}
            deleteMessage={deleteMessage}
          />
          <MessageInput
            editedMessage={editedMessage}
            addNewMessage={addNewMessage}
            setMessageToEdit={setMessageToEdit}
            editMessage={editMessage}
          />
        </>
      )}
    </div>
  );
};

Chat.propTypes = {
  url: propTypes.string.isRequired,
};

export default Chat;
