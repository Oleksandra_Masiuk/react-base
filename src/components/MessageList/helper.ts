const getDate = (oldDate: string): string => {
  const from = new Date();
  const date = new Date(oldDate);

  const difference = Math.floor(
    (from.getTime() - date.getTime()) / (1000 * 3600 * 24)
  );

  if (difference === 0) {
    return 'Today';
  }
  if (difference === 1) {
    return 'Yesterday';
  }

  let dayNumber = date.getDate().toString();

  if (date.getDate() < 10) {
    dayNumber = '0' + dayNumber;
  }

  const month = date.toLocaleString('en-US', { month: 'long' });
  const day = date.toLocaleString('en-US', { weekday: 'long' }) + ',';

  return [day, dayNumber, month].join(' ');
};

const groupBy = (array: [any], key: string): object => {
  return array.reduce((result, currentItem) => {
    let date: number = new Date(currentItem[key]).getTime();
    date = Math.floor(date / (1000 * 60 * 60 * 24));
    (result[date] = result[date] || []).push(currentItem);
    return result;
  }, {});
};

export { getDate, groupBy };
