import { useRef, useEffect } from 'react';
import propTypes from 'prop-types';
import { ID } from '../../constants/currentUser';

import './MessageList.css';
import Message from '../Message/Message';
import { getDate, groupBy } from './helper';
import { IMessage } from '../../interfaces/interfaces';
import OwnMessage from '../OwnMessage/OwnMessage';

export default function MessageList({
  messages,
  editedMessage,
  setMessageToEdit,
  deleteMessage,
}) {
  const groupedMessages = groupBy(messages, 'createdAt');
  const list = useRef<HTMLDivElement>(null);

  useEffect(() => {
    list.current?.lastElementChild?.lastElementChild?.scrollIntoView({
      behavior: 'smooth',
    });
  }, [messages]);

  return (
    <div ref={list} className="message-list">
      {Object.keys(groupedMessages).map((key) => (
        <div key={groupedMessages[key][0].id + groupedMessages[key][0].userId}>
          <div className="messages-divider">
            <div className="messages-divider-date">
              {getDate(groupedMessages[key][0].createdAt)}
            </div>
          </div>
          {groupedMessages[key].map((message: IMessage) =>
            message.userId === ID ? (
              <OwnMessage
                key={message.id}
                message={message}
                editedMessage={editedMessage}
                setMessageToEdit={setMessageToEdit}
                deleteMessage={deleteMessage}
              ></OwnMessage>
            ) : (
              <Message message={message} key={message.id} />
            )
          )}
        </div>
      ))}
    </div>
  );
}

MessageList.propTypes = {
  messages: propTypes.arrayOf(
    propTypes.shape({
      avatar: propTypes.string,
      createdAt: propTypes.string.isRequired,
      editedAt: propTypes.string,
      id: propTypes.string.isRequired,
      text: propTypes.string.isRequired,
      user: propTypes.string.isRequired,
      userId: propTypes.string.isRequired,
    })
  ).isRequired,
  editedMessage: propTypes.string,
  setMessageToEdit: propTypes.func.isRequired,
  deleteMessage: propTypes.func.isRequired,
};
