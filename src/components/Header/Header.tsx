import { FC } from 'react';

import { CHAT_NAME } from '../../constants/chat';
import { setDateFormat } from './helper';
import './Header.css';

interface HeaderProps {
  userCounter: number;
  messageCounter: number;
  lastMessageDate: string;
}

const Header: FC<HeaderProps> = ({
  userCounter,
  messageCounter,
  lastMessageDate,
}) => {
  return (
    <div className="header">
      <div className="header-title">{CHAT_NAME}</div>
      <div className="header-users-count-wrap">
        <div className="header-users-count">{userCounter}</div>
        <span>participants</span>
      </div>
      <div className="header-messages-count-wrap">
        <div className="header-messages-count">{messageCounter}</div>
        <span>messages</span>
      </div>
      <div className="header-last-message-date-wrap">
        <span>last message</span>
        <div className="header-last-message-date">
          {setDateFormat(lastMessageDate)}
        </div>
      </div>
    </div>
  );
};

export default Header;
