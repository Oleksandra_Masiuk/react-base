const setDateFormat = (oldDate: string): string => {
  if (!oldDate) return '';
  const date = new Date(oldDate);

  let month = date.getMonth().toString();
  let hours = date.getHours().toString();
  let minutes = date.getMinutes().toString();
  let day = date.getDate().toString();
  if (date.getMonth() + 1 < 10) {
    month = '0' + (date.getMonth() + 1);
  }
  if (date.getHours() < 10) {
    hours = '0' + hours;
  }
  if (date.getMinutes() < 10) {
    minutes = '0' + minutes;
  }
  if (date.getDate() < 10) {
    day = '0' + day;
  }

  const dateFormat = [day, month, date.getFullYear()].join('.');
  const timeFormat = [hours, minutes].join(':');

  return `${dateFormat} ${timeFormat}`;
};
export { setDateFormat };
