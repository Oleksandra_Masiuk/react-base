const setMessageDateFormat = (oldDate: string) => {
  if (!oldDate) return '';
  const date = new Date(oldDate);

  let hours = date.getHours().toString();
  let minutes = date.getMinutes().toString();
  if (date.getHours() < 10) {
    hours = '0' + hours;
  }
  if (date.getMinutes() < 10) {
    minutes = '0' + minutes;
  }
  const timeFormat = [hours, minutes].join(':');

  return timeFormat;
};
export { setMessageDateFormat };
