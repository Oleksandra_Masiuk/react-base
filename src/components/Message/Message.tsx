import { FC } from 'react';
import { useState } from 'react';

import './Message.css';
import { setMessageDateFormat } from './helper';
import { IMessage } from '../../interfaces/interfaces';

interface MessageProps {
  message: IMessage;
}

const Message: FC<MessageProps> = ({ message }) => {
  const [isLiked, setIsLiked] = useState(false);
  return (
    <div className="message">
      <div className="message-header">
        <img
          className="message-user-avatar"
          src={message.avatar}
          alt="avatar"
        />
        <div className="message-user-name">{message.user}</div>
        <div className="message-time">
          <span>{setMessageDateFormat(message.createdAt)}</span>
          {message.editedAt && (
            <span className="message-time-edited">
              {'edited at ' + setMessageDateFormat(message.editedAt)}
            </span>
          )}
        </div>
        <button
          className={isLiked ? 'message-liked' : 'message-like'}
          onClick={() => {
            setIsLiked(!isLiked);
          }}
        ></button>
      </div>
      <div className="message-text">{message.text}</div>
    </div>
  );
};

export default Message;
