import { setMessageDateFormat } from '../Message/helper';
import { useState, FC } from 'react';

import DeleteModal from '../Modal/Modal';
import { IMessage } from '../../interfaces/interfaces';
import './OwnMessage.css';

interface OwnMessageProps {
  message: IMessage;
  setMessageToEdit: (id: string | null | undefined) => void;
  deleteMessage: (id: string) => void;
  editedMessage?: string;
}

const OwnMessage: FC<OwnMessageProps> = ({
  message,
  setMessageToEdit,
  deleteMessage,
  editedMessage,
}) => {
  const [showModal, setShowModal] = useState(false);

  const onDelete = () => {
    deleteMessage(message.id);
    setShowModal(false);
  };

  return (
    <>
      <div className="own-message">
        <div className="message-time">
          <span>{setMessageDateFormat(message.createdAt)}</span>
          {message.editedAt && (
            <span className="message-time-edited">
              {'edited at ' + setMessageDateFormat(message.editedAt)}
            </span>
          )}
        </div>
        <div className="message-text">{message.text}</div>
        {editedMessage !== message.id && (
          <div className="buttons">
            <button
              className="message-edit"
              onClick={() => {
                setMessageToEdit(message.id);
              }}
            >
              Edit
            </button>
            <button
              className="message-delete"
              onClick={() => setShowModal(true)}
            >
              Delete
            </button>
          </div>
        )}
      </div>
      {showModal && (
        <DeleteModal
          onClose={() => setShowModal(false)}
          onDelete={() => onDelete()}
        />
      )}
    </>
  );
};

export default OwnMessage;
