import { FC } from 'react';
import './MainHeader.css';

const MainHeader: FC = () => {
  return (
    <header className="main-header">
      <span className="message-like"></span>
      <span className="header-title">Chats</span>
    </header>
  );
};
export default MainHeader;
