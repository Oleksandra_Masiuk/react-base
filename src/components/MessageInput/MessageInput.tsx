import { useEffect, useState, FC } from 'react';

import { ID, USER, AVATAR } from '../../constants/currentUser';
import { IMessage } from '../../interfaces/interfaces';
import './MessageInput.css';

interface DeleteModalProps {
  editedMessage: IMessage;
  addNewMessage: (message: IMessage) => void;
  editMessage: (text: string) => void;
  setMessageToEdit: (id: string | null | undefined) => void;
}

const MessageInput: FC<DeleteModalProps> = ({
  editedMessage,
  addNewMessage,
  editMessage,
  setMessageToEdit,
}) => {
  const [text, setText] = useState('');

  const addMessage = () => {
    if (text.trim() === '') return;
    const message = {
      userId: ID,
      avatar: AVATAR,
      user: USER,
      text: text,
      createdAt: new Date().toString(),
      editedAt: '',
      id: '',
    };
    addNewMessage(message);
    setText('');
  };

  const updateMessage = (): void => {
    if (text.trim() === '') return;
    editMessage(text);
  };

  useEffect(() => {
    setText(editedMessage ? editedMessage.text : '');
  }, [editedMessage]);

  return (
    <div className="message-input">
      <input
        value={text}
        className="message-input-text"
        placeholder="Enter your message"
        onChange={(e) => {
          setText(e.target.value);
        }}
      />
      <div className="button-wrapper">
        <button
          className="message-input-button"
          onClick={() => {
            editedMessage ? updateMessage() : addMessage();
          }}
        >
          {editedMessage ? 'Edit' : 'Send'}
        </button>
        {editedMessage && (
          <>
            <div className="between-buttons"></div>
            <button
              className="message-cancel-button"
              onClick={() => {
                setMessageToEdit(null);
              }}
            >
              Cancel
            </button>
          </>
        )}
      </div>
    </div>
  );
};

export default MessageInput;
