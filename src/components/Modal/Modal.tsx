import { FC } from 'react';
import { createPortal } from 'react-dom';
import './Modal.css';

interface DeleteModalProps {
  onClose: () => void;
  onDelete: () => void;
}

const modalRoot = document.querySelector('#modal-root');

const DeleteModal: FC<DeleteModalProps> = ({ onClose, onDelete }) => {
  return createPortal(
    <div className="backdrop">
      <div className="modal">
        Do you want to delete message?
        <div className="modal-buttons">
          <button className="modal-button-delete" onClick={onDelete}>
            Yes
          </button>
          <button className="modal-button-cancel" onClick={onClose}>
            No
          </button>
        </div>
      </div>
    </div>,
    modalRoot
  );
};

export default DeleteModal;
