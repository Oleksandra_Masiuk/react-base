const ID = 'a2395f64-d59f-4a23-9e23-9349f2d7bf80';
const USER = 'Oleksandra';
const AVATAR =
  'https://resizing.flixster.com/IaXbRF4gIPh9jireK_4VCPNfdKc=/300x0/v2/https://flxt.tmsimg.com/v2/AllPhotos/591658/591658_v2_bb.jpg';

export { ID, USER, AVATAR };
