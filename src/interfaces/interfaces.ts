interface IMessage {
  avatar: string | null;
  createdAt: string;
  editedAt: string | null;
  id: string | null | undefined;
  text: string;
  user: string;
  userId: string;
}

export { IMessage };
